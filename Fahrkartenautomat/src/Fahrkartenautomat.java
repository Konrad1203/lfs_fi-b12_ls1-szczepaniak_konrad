﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
	}

	public static double fahrkartenbestellungErfassen() {
		int anzahl;
		double einzelPreis;
		double zuZahlenderBetrag;

		Scanner tastatur = new Scanner(System.in);

		System.out.println("Fahrkartenbestellvorgang: ");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		System.out.println("\n\n");

		System.out.println(" Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: \n"
				+ " Einzelfahrschein Regetarif AB [2.90 EUR] (1) \n" + " Tageskarte Regeltarif AB [8,60 EUR] (2) \n"
				+ " Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3) \n");
		for (int i = 0; i < 8; i++) {
			System.out.print(" ");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		System.out.println("\n\n");

		public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
			}
		}
		
		/*System.out.print("Zu zahlender Betrag (Euro): ");
		einzelPreis = tastatur.nextDouble();
		if (einzelPreis <= 0) {
			System.out.println("Ticketpreis kann nich negetiv sein");
		}

		System.out.print("Anzahl der Tickets: ");
		anzahl = tastatur.nextInt();
		if (anzahl >= 10) {
			System.out.println("Sie können nicht mehr als 10 Tickets kaufen");
		}

		zuZahlenderBetrag = anzahl * einzelPreis;

		return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingeworfeneMünze;
		double eingezahlterGesamtbetrag = 0.0;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: " + "%.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag;
	}*/

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		System.out.println("\n\n");
	}

	public static double rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f Euro\n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 Euro");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 Euro");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 Cent");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 Cent");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 Cent");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 Cent");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");

		return rückgabebetrag;
	}
}
